package cat.dam.edgar.majormenorigual;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    private static int nElefants;
    private static int nLleons;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int nElefants, nLleons;
        final ImageButton leftButton = findViewById(R.id.ib_dreta_petit);
        final ImageButton rightButton = findViewById(R.id.ib_esquerre_petit);
        final ImageButton middle = findViewById(R.id.ib_igual);
        final ImageButton restart = findViewById(R.id.ib_restart);

        newGame();

        // dreta = Lleó grans | esquerre = Elefants grans
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(MainActivity.nElefants, MainActivity.nLleons, "esquerre");
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(MainActivity.nElefants, MainActivity.nLleons, "dreta");
            }
        });

        middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(MainActivity.nElefants, MainActivity.nLleons, "iguals");
            }
        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newGame();
            }
        });

    }

    protected void newGame()
    {
        Random r = new Random();
        MainActivity.nElefants = r.nextInt(10)+1;
        MainActivity.nLleons = r.nextInt(10)+1;

        deleteGrids();
        createImages(nElefants, "elefant");
        createImages(nLleons, "lion");
    }

    protected void deleteGrids()
    {
        ConstraintLayout mainLayout = findViewById(R.id.cl_main);
        GridLayout lionLayout = findViewById(R.id.gl_lion_id);
        GridLayout elefantLayout = findViewById(R.id.gl_elefant_id);

        mainLayout.removeView(lionLayout);
        mainLayout.removeView(elefantLayout);
    }

    protected void checkAnswer(int nElefants, int nLleons, String answer)
    {
        int numCorrect = checkAnimalNum(nElefants, nLleons);

        createToast(checkIsCorrect(numCorrect, answer));
    }

    protected boolean checkIsCorrect(int numCorrect, String answer)
    {
        if((numCorrect == 1) && answer.equals("dreta")) return true;
        if((numCorrect == 2) && answer.equals("iguals")) return true;

        return (numCorrect == 3) && answer.equals("esquerre"); // If correct return true else return false
    }

    protected int checkAnimalNum(int nElefants, int nLleons)
    {
        // 1 = Elefants petits | 2 = Iguals | 3 = Lleons petits
        if(nElefants<nLleons) return 1;
        if(nElefants == nLleons) return 2;
        return 3;
    }

    protected void createToast(boolean answer)
    {
        String message = "Correcte, l'has encertat!";

        if(!answer) message = "Resposta incorrecte, torna a provar!";
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void createImages(int nImages, String animal)
    {
        GridLayout grid = createGrid(nImages, animal);
        ConstraintLayout mainLayout = findViewById(R.id.cl_main);
        for (int i = 1; i <= nImages; i++) {
            ImageView image = createImageView(animal);
            image = setImageParams(image, nImages, animal);
            grid.addView(image);
        }
        mainLayout.addView(grid);
        setConstraints(grid, animal, mainLayout);
    }

    protected ImageView createImageView(String animal)
    {
        ImageView image = new ImageView(getApplicationContext());
        Drawable animalImage;

        if(animal.equals("elefant")) animalImage = getDrawable(R.drawable.elefant);
        else animalImage = getDrawable(R.drawable.lion);

        image.setImageDrawable(animalImage);

        return image;
    }

    protected ImageView setImageParams(ImageView image, int nAnimals, String animal)
    {
        int id;

        if(animal.equals("elefant")) id = R.id.elefant_image;
        else id = R.id.lion_image;

        image.setId(id);
        image.setLayoutParams(getImageParams(nAnimals));

        return image;
    }

    protected GridLayout.LayoutParams getImageParams(int nAnimals)
    {
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();

        if(nAnimals == 1) {
            params.width = 200;
            params.height = 200;
        } else {
            params.width = 122;
            params.height = 132;
        }

        return params;
    }

    // Create GridLayout
    protected GridLayout createGrid(int nAnimals, String animal)
    {
        GridLayout grid = new GridLayout(getApplicationContext());
        int columns, rows, margin = 10, id;

        if(nAnimals == 1){ columns = rows = nAnimals; }
        else if (nAnimals == 2){ rows = 1; columns = nAnimals; }
        else if (nAnimals>6){ rows = 3; columns = nAnimals/3; }
        else {
            rows = 2;
            if(nAnimals%2==0) columns = nAnimals/2;
            else columns = (nAnimals/2)+1;
        }

        if(animal.equals("elefant")) id = R.id.gl_elefant_id;
        else id = R.id.gl_lion_id;

        grid = setParameters(grid, id, columns, rows);

        return grid;
    }

    // Set GridLayout Parameters
    protected GridLayout setParameters(GridLayout grid, int id, int columns, int rows)
    {
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();

        grid.setId(id);
        grid.setColumnCount(columns);
        grid.setRowCount(rows);

        params.width = GridLayout.LayoutParams.WRAP_CONTENT;
        params.height = GridLayout.LayoutParams.WRAP_CONTENT;

        grid.setLayoutParams(params);

        return grid;
    }

    // Set GridLayout Constraints
    protected void setConstraints(GridLayout grid, String animal, ConstraintLayout layout)
    {
        ConstraintSet set = new ConstraintSet();
        ImageButton left = findViewById(R.id.ib_dreta_petit);
        ImageButton right = findViewById(R.id.ib_esquerre_petit);
        int idGrid = grid.getId(), idMain = layout.getId(), idLeft = left.getId(), idRight = right.getId();

        set.clone(layout);

        if(animal.equals("elefant")){
            set.connect(idGrid, ConstraintSet.BOTTOM, idLeft, ConstraintSet.TOP, 120);
            set.connect(idGrid, ConstraintSet.LEFT, idMain, ConstraintSet.LEFT, 100);
        } else {
            set.connect(idGrid, ConstraintSet.BOTTOM, idRight, ConstraintSet.TOP, 120);
            set.connect(idGrid, ConstraintSet.RIGHT, idMain, ConstraintSet.RIGHT, 100);
        }

        set.applyTo(layout);
    }
}